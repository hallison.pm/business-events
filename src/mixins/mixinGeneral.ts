import { Component, Vue, Prop } from "vue-property-decorator";
@Component
export class MixinGeneral extends Vue {
    @Prop({ type: String, default: "250px" }) height: any;
    @Prop({ type: String, default: "250px" }) width: any;
    @Prop({ type: String, default: "0px" }) marginRight: any;
    @Prop({ type: String, default: "0px" }) marginBottom: any;
    hexToRgb(hex: any) {
        try {
            const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result
                ? `rgba(${parseInt(result[1], 16)},${parseInt(
                    result[2],
                    16
                )},${parseInt(result[3], 16)},1)`
                : hex;
        } catch (error) {
            return hex;
        }
    }

    setProps(Gradient: HTMLElement) {
        Gradient.style.height = this.height;
        Gradient.style.width = this.width;
        Gradient.style.marginRight = this.marginRight;
        Gradient.style.marginBottom = this.marginBottom;
    }
}
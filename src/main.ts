import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'

import "./assets/scss/index.scss"

Vue.config.productionTip = false;
(Vue as any).root = {
  color1: process.env.VUE_APP_COLOR1,
  color2: process.env.VUE_APP_COLOR2,
  color3: process.env.VUE_APP_COLOR3,
  color4: process.env.VUE_APP_COLOR4,
}
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
